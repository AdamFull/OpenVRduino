#pragma once

#include <Arduino.h>
#include <gyro_config.h>

typedef union accel_t_gyro_union
{
  struct
  {
    uint8_t x_accel_h;
    uint8_t x_accel_l;
    uint8_t y_accel_h;
    uint8_t y_accel_l;
    uint8_t z_accel_h;
    uint8_t z_accel_l;
    uint8_t t_h;
    uint8_t t_l;
    uint8_t x_gyro_h;
    uint8_t x_gyro_l;
    uint8_t y_gyro_h;
    uint8_t y_gyro_l;
    uint8_t z_gyro_h;
    uint8_t z_gyro_l;
  } reg;
  struct 
  {
    int x_accel;
    int y_accel;
    int z_accel;
    int temperature;
    int x_gyro;
    int y_gyro;
    int z_gyro;
  } value;
};

class GYRO
{
public:
    GYRO()
    {
        
    }
    bool init(int serial_speed);
    void calibrate_sensors();
    int MPU6050_write(int start, const uint8_t *pData, int size);
    int MPU6050_write_reg(int reg, uint8_t data);
    int MPU6050_read(int start, uint8_t *buffer, int size);
    void set_last_read_angle_data(unsigned long time, float x, float y, float z, float x_gyro, float y_gyro, float z_gyro);
    inline unsigned long get_last_time() {return last_read_time;}
    inline float get_last_x_angle() {return last_x_angle;}
    inline float get_last_y_angle() {return last_y_angle;}
    inline float get_last_z_angle() {return last_z_angle;}
    inline float get_last_gyro_x_angle() {return last_gyro_x_angle;}
    inline float get_last_gyro_y_angle() {return last_gyro_y_angle;}
    inline float get_last_gyro_z_angle() {return last_gyro_z_angle;}
    inline float get_base_x_gyro() {return base_x_gyro;}
    inline float get_base_y_gyro() {return base_y_gyro;}
    inline float get_base_z_gyro() {return base_z_gyro;}
    inline float get_accel_angle_x() {return accel_angle_x;}
    inline float get_accel_angle_y() {return accel_angle_y;}
    inline float get_accel_angle_z() {return accel_angle_z;}
    inline float get_gyro_angle_x() {return gyro_angle_x;}
    inline float get_gyro_angle_y() {return gyro_angle_y;}
    inline float get_gyro_angle_z() {return gyro_angle_z;}
    inline float get_filtred_angle_x() {return angle_x;}
    inline float get_filtred_angle_y() {return angle_y;}
    inline float get_filtred_angle_z() {return angle_z;}
    int read_gyro_accel_vals(uint8_t* accel_t_gyro_ptr);
    void calculate_loop_gyro_data();
    
    double get_gyro_temperature();

private:
    unsigned long last_read_time;
    float last_x_angle;  // These are the filtered angles
    float last_y_angle;
    float last_z_angle;  
    float last_gyro_x_angle;  // Store the gyro angles to compare drift
    float last_gyro_y_angle;
    float last_gyro_z_angle;

    float base_x_accel;
    float base_y_accel;
    float base_z_accel;

    float base_x_gyro;
    float base_y_gyro;
    float base_z_gyro;

    float accel_angle_x;
    float accel_angle_y;
    float accel_angle_z;
    float gyro_angle_x;
    float gyro_angle_y;
    float gyro_angle_z;

    float angle_x;
    float angle_y;
    float angle_z;

    int error;
    accel_t_gyro_union accel_t_gyro;
    // Get angle values from accelerometer
    float RADIANS_TO_DEGREES = 180/3.14159;

    double dT;

    float FS_SEL = 131;
};