#include <configuration.h>

#if defined(TRANSMITTER)
  #include <transmitter.h>
#elif defined(RECEIVER)
  #include <receiver.h>
#else
  #include <test_radio.h>
#endif