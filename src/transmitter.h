#include <Arduino.h>
#include <configuration.h>

#if defined(MPU6050_SENSOR)
    #include <I2Cdev.h>
    #include <MPU6050_6Axis_MotionApps20.h>
#endif

#if I2CDEV_IMPLEMENTATION == I2CDEV_ARDUINO_WIRE
    #include "Wire.h"
#endif

#include <SPI.h>
#include <RF24.h>
#include <nondelay_timer.h>

MPU6050 mpu;
RF24 transmitter(9, 10);
ndelay timer(400);

int error_counter = 0;  // fatal errors counter
String error_buffer[10];// containing fatal errors

bool blink_state = false;
uint8_t mpuIntStatus;   // holds actual interrupt status byte from MPU
uint8_t devStatus;      // return status after each device operation (0 = success, !0 = error)
uint16_t packetSize;    // expected DMP packet size (default is 42 bytes)
uint16_t fifoCount;     // count of all bytes currently in FIFO
uint8_t fifoBuffer[64]; // FIFO storage buffer

Quaternion q;           // [w, x, y, z]         quaternion container
VectorFloat gravity;    // [x, y, z]            gravity vector
float ypr[3];           // [yaw, pitch, roll]   yaw/pitch/roll container and gravity vector

volatile bool mpuInterrupt = false;     // indicates whether MPU interrupt pin has gone high
void dmpDataReady() 
{
    mpuInterrupt = true;
}

void setup()
{ 
    #if I2CDEV_IMPLEMENTATION == I2CDEV_ARDUINO_WIRE
        Wire.begin();
        Wire.setClock(400000); // 400kHz I2C clock. Comment this line if having compilation difficulties
    #elif I2CDEV_IMPLEMENTATION == I2CDEV_BUILTIN_FASTWIRE
        Fastwire::setup(400, true);
    #endif

    #if defined(DEBUG)
        Serial.begin(BAUDRATE);
    #endif

    while (!Serial);

    //------------GYRO INITIALIZE-------------
    mpu.initialize();
    devStatus = mpu.dmpInitialize();
    mpu.setXGyroOffset(164);
    mpu.setYGyroOffset(-151);
    mpu.setZGyroOffset(-16);
    mpu.setXAccelOffset(-2174);
    mpu.setYAccelOffset(-162); 
    mpu.setZAccelOffset(785);
    //---------------END REGION---------------

    //-------------IO INITIALIZE--------------
    pinMode(FACE_D_BTN, INPUT_PULLUP);          //xbox x/a button
    pinMode(FACE_U_BTN, INPUT_PULLUP);          //xbox y/b button
    pinMode(SERVICE_BTN, INPUT_PULLUP);         //select/start button
    pinMode(BUMPER_BTN, INPUT_PULLUP);          //bumper button
    pinMode(TRIGGER_BTN, INPUT_PULLUP);         //trigger button
    pinMode(STICK_BTN, INPUT_PULLUP);           //stick trigger
    pinMode(STICK_LEFT_RIGHT, INPUT);           //analogue stick lr
    pinMode(STICK_UP_DOWN, INPUT);              //analogue stick ud
    pinMode(LED_PIN, OUTPUT);
    pinMode(INTERRUPT_PIN, INPUT);
    //---------------END REGION---------------

    //------------RADIO INITIALIZE------------
    if(!transmitter.begin())
    {
        error_buffer[error_counter] = "ERR: Unable access to RF24. Please check your radio connection.";
        error_counter +=1;
    }
    transmitter.setPALevel(RF24_PA_LOW);
    transmitter.openWritingPipe(RADIO_ADDRESS);
    transmitter.startListening();
    //---------------END REGION---------------

    if (devStatus == 0)
    {
        mpu.setDMPEnabled(true);
        attachInterrupt(digitalPinToInterrupt(INTERRUPT_PIN), dmpDataReady, RISING);
        mpuIntStatus = mpu.getIntStatus();
        packetSize = mpu.dmpGetFIFOPacketSize();
    } 
    else 
    {
        error_buffer[error_counter] = "ERR: Unable access to gyroscope. Please check the board.";
        error_counter += 1;
    }
}


void loop()
{
    if (error_counter > 0)
    {
        for( unsigned i = 0; i < error_counter; i++)
        Serial.println(error_buffer[i]);
        while(true)
        {
            if(timer.abuse())
            {
                blink_state = !blink_state;
                digitalWrite(LED_PIN, blink_state);     //Error indication
            }
        }
    }

    while (!mpuInterrupt && fifoCount < packetSize) 
    {
        if (mpuInterrupt && fifoCount < packetSize)
        {
        // try to get out of the infinite loop 
        fifoCount = mpu.getFIFOCount();
        }  
    }

    // reset interrupt flag and get INT_STATUS byte
    mpuInterrupt = false;
    mpuIntStatus = mpu.getIntStatus();

    // get current FIFO count
    fifoCount = mpu.getFIFOCount();

    if ((mpuIntStatus & _BV(MPU6050_INTERRUPT_FIFO_OFLOW_BIT)) || fifoCount >= 1024)
    {
        // reset so we can continue cleanly
        mpu.resetFIFO();
        fifoCount = mpu.getFIFOCount();
        #if defined(DEBUG)
        Serial.println(F("FIFO overflow!"));
        #endif
    }
    else if (mpuIntStatus & _BV(MPU6050_INTERRUPT_DMP_INT_BIT)) 
    {
        // wait for correct available data length, should be a VERY short wait
        while (fifoCount < packetSize) fifoCount = mpu.getFIFOCount();

        // read a packet from FIFO
        mpu.getFIFOBytes(fifoBuffer, packetSize);
            
        // track FIFO count here in case there is > 1 packet available
        // (this lets us immediately read more without waiting for an interrupt)
        fifoCount -= packetSize;

        // display Euler angles in degrees
        mpu.dmpGetQuaternion(&q, fifoBuffer);
        mpu.dmpGetGravity(&gravity, &q);
        mpu.dmpGetYawPitchRoll(ypr, &q, &gravity);
        #if defined(DEBUG)
            Serial.print("ypr\t");
            Serial.print(map(ypr[0] * 180/M_PI, -180, 180, 0, 1023));
            Serial.print("\t");
            Serial.print(map(ypr[1] * 180/M_PI * -1, -180, 180, 0, 1023));
            Serial.print("\t");
            Serial.println(map(ypr[2] * 180/M_PI, -180, 180, 0, 1023));
        #endif
        
        //Add the bit of the controller number(left/right - check configuration.h)
        ctrl.Initialization = CONTROLLER_NUMBER_BIT;
        //Clear previous bytes
        ctrl.Pressed = CLEAR_BYTE;
        ctrl.Yaw = CLEAR_BYTE;
        ctrl.Pitch = CLEAR_BYTE;
        ctrl.Roll = CLEAR_BYTE;
        ctrl.Stick_UD = CLEAR_BYTE;
        //Write buttons state
        ctrl.Pressed += !digitalRead(FACE_D_BTN) ? GP_FACE_D : CLEAR_BYTE;
        ctrl.Pressed += !digitalRead(FACE_U_BTN) ? GP_FACE_U: CLEAR_BYTE;
        ctrl.Pressed += !digitalRead(TRIGGER_BTN) ? GP_TRIGGER: CLEAR_BYTE;
        ctrl.Pressed += !digitalRead(BUMPER_BTN) ? GP_SHOULDER: CLEAR_BYTE;
        ctrl.Pressed += !digitalRead(STICK_BTN) ? GP_STICK: CLEAR_BYTE;
        ctrl.Pressed += !digitalRead(SERVICE_BTN) ? GP_SERVICE: CLEAR_BYTE;

        //write gyro values 10*3 bits
        //Yaw
        ctrl.Yaw      = map(ypr[0] * 180/M_PI, -180, 180, 0, 1023);
        //Pitch
        ctrl.Pitch    = map(ypr[1] * 180/M_PI * -1, -180, 180, 0, 1023);
        //Roll
        ctrl.Roll     = map(ypr[2] * 180/M_PI, -180, 180, 0, 1023);

        //write analog data
        //stick lr
        ctrl.Stick_LR = analogRead(STICK_LEFT_RIGHT);
        //stick ud
        ctrl.Stick_UD = analogRead(STICK_UP_DOWN);

        
        //Output binary
        transmitter.stopListening();
        transmitter.write((byte*)&ctrl, sizeof(ctrl));
        transmitter.startListening();
        
        // blink LED to indicate activity
        digitalWrite(LED_PIN, true);
    }
}