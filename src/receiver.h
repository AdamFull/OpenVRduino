#include <Arduino.h>
#include <SPI.h>
#include <RF24.h>
#include <configuration.h>

RF24 receiver(9, 10);

int error_counter = 0;  // fatal errors counter
String error_buffer[10];// containing fatal errors

void setup()
{
    #if defined(DEBUG)
        Serial.begin(BAUDRATE);
    #endif

    if(!receiver.begin())
    {
        error_buffer[error_counter] = "ERR: Unable access to RF24. Please check your radio connection.";
        error_counter +=1;
    }
    receiver.setPALevel(RF24_PA_LOW);
    receiver.openListeningPipe(RADIO_CHANNEL, RADIO_ADDRESS);
    receiver.startListening();
}

void loop()
{
    receiver.read(ctrl, sizeof(ctrl));
    Serial.write((byte*)&ctrl, sizeof(ctrl));
}