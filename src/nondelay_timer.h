#pragma once

#include "Arduino.h"

class ndelay
{
public:
    ndelay(unsigned long frequence) 
    { 
        this->frequence = frequence;
        c_time = 0;
    }

    bool abuse()
    {
        if(millis() - c_time > frequence)
        {
            c_time = millis();
            return true;
        }
        else
            return false;
    }
    void setFreqyence(unsigned long frequence)
    {
        this->frequence = frequence;
    }

    void setEnabled(bool enabled)
    {
        this->enabled = enabled;
    }

private:
    unsigned long frequence;
    unsigned long c_time;
    bool enabled = true;
};