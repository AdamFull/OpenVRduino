#pragma once

#include "Arduino.h"

//--------------------------------------
//--------------DEVICE TYPE-------------
//--------------------------------------
#define TRANSMITTER
//#define RECEIVER
//--------------------------------------

//--------------------------------------
//------------STANDART SETUP------------
//--------------------------------------
#define SERIAL_PORT         0x0
#define BAUDRATE            0x1C200
#define DEBUG
#define RADIO_CHANNEL       0x0
#define RADIO_ADDRESS       0x00CA
//--------------------------------------

struct bit_field        //60bit
{
    unsigned Initialization:    0x4;
    unsigned Pressed:           0x6;
    unsigned Yaw:               0xA;
    unsigned Pitch:             0xA;
    unsigned Roll:              0xA;
    unsigned Stick_LR:          0xA;
    unsigned Stick_UD:          0xA;
} ctrl;

#if defined(TRANSMITTER)

    //--------------------------------------
    //---------------PAD TYPE---------------
    //--------------------------------------
    #define LEFT_PAD                    //left hand setup
    //#define RIGHT_PAD                 //right hand setup
    //#define USE_CROSS
    //--------------------------------------

    //--------------------------------------
    //----------MOTION SENSOR TYPE----------
    //--------------------------------------
    #define MPU6050_SENSOR
    //#define MPU9255_SENSOR
    //#define MPU9250_SENSOR

    //--------------------------------------
    //-------------BUTTON SETUP-------------
    //--------------------------------------

    #define FACE_D_BTN          0x0003                 //xbox x/a button
    #define FACE_U_BTN          0x0004                 //xbox y/b button
    #define SERVICE_BTN         0x0005                 //select/start button
    #define BUMPER_BTN          0x0006                 //bumper button
    #define TRIGGER_BTN         0x0007                 //trigger button
    #define STICK_BTN           0x0008                 //stick trigger
    #define STICK_LEFT_RIGHT    A0                     //analogue stick lr
    #define STICK_UP_DOWN       A1                     //analogue stick ud

    //--------------------------------------
    //----------------UNUSUAL---------------
    //--------------------------------------
    #if defined(USE_CROSS)
        #define CROSS_LEFT 8
        #define CROSS_RIGHT 9
        #define CROSS_UP 10
        #define CROSS_DOWN 11
    #endif
    //-----------END BUTTON SETUP-----------

    //--------------------------------------
    //----------ADDITIONAL PINOUT-----------
    //--------------------------------------
    #define LED_PIN             0xD           //pin for ping-pong led ball
    #define INTERRUPT_PIN       0x2           //pin for interrupt


    #if defined(LEFT_PAD)
        #define CONTROLLER_NUMBER_BIT 0b00000000
    #elif defined(RIGHT_PAD)
        #define CONTROLLER_NUMBER_BIT 0b00000001
    #else
        #define CONTROLLER_NUMBER_BIT 0b00000010
    #endif

    //--------------------------------------
    //--------------BIT CODES---------------
    //-----------DON'T TOUCH THIS-----------
    //--------------------------------------
    #define GP_FACE_D       0b000001
    #define GP_FACE_U       0b000010
    #define GP_SERVICE      0b000100
    #define GP_SHOULDER     0b001000
    #define GP_TRIGGER      0b010000
    #define GP_STICK        0b100000
    #define CLEAR_BYTE      0b000000
#else

#endif